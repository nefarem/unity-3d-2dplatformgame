﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    // Ukończenie gry gdy gracz dotrze do odpowiedniego miejsca 
    public GameObject WinGamePanel;

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) WinGamePanel.SetActive(true);
    }
}
