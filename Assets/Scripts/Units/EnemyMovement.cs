﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    Enemy enemy;
    Animator animator;
    SpriteRenderer spriteRenderer;

    GameObject waypointTarget; // Waypoint do które ma udać się NPC
    GameObject collisionWaypoint; // Waypoint do którego dotarł NPC

    float speed = 0.6f;

    bool goToNearWaypoint; // Czy idzie do najbliższego waypointu
    bool waypointFinded; // Czy NPC dotarł do najbliższego waypointu i może patrolować między 2 waypointami

    void Start()
    {
        enemy = GetComponent<Enemy>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        waypointTarget = enemy.FindNearWaypoint();  // Jako target ustawiany jest na początku ten waypoint który został znaleziony najbliżej NPC przy pomocy metody 'FindNearWaypoint'
    }

    void Update()
    {
        Move();
    }


    // NPC porusza się tylko gdy NPCAction to patrol
    // Jeżeli NPC nie patroluje międzzy dwoma waypointami to udaje się najpierw do najbliższego
    // Gdy NPC dotrze do najbliższego waypointa pobierze z niiego następnego waypointa i tak w kółko (patrol między dwoma waypointami)
    void Move()
    {
        if (enemy.detectPlayer == false && enemy.isDead == false && enemy.isAttacking == false && enemy.NPCActionEnumType == Enemy.NPCAction.patrol)
        {
            if (enemy.detectPlayer == false && goToNearWaypoint == false && waypointFinded == false) goToNearWaypoint = true;

            if (enemy.detectPlayer == false && goToNearWaypoint)
            {
                transform.position = Vector2.MoveTowards(gameObject.transform.position, waypointTarget.transform.position, Time.deltaTime * speed);

                // Odwrócenie sprite'a wzdłuż osi X
                SetFlipX(waypointTarget);
            }


            if (Vector2.Distance(gameObject.transform.position, waypointTarget.transform.position) <= 0.3f)
            {
                goToNearWaypoint = false;
                waypointFinded = true;

                waypointTarget = collisionWaypoint.GetComponent<Waypoint>().nextWaypoint;

                SetFlipX(waypointTarget);
            }
            else
            {
                animator.SetInteger("AnimState", 2);

                transform.position = Vector2.MoveTowards(gameObject.transform.position, waypointTarget.transform.position, Time.deltaTime * speed);
            }
        }

        // Gdy gracz zbliży się do NPCta który patroluje, ten zacznie go gonić
        if (enemy.detectPlayer && enemy.isDead == false && enemy.isAttacking == false && enemy.NPCActionEnumType != Enemy.NPCAction.stay)
        {
            animator.SetInteger("AnimState", 2);

            transform.position = Vector2.MoveTowards(transform.position, enemy.target.transform.position, Time.deltaTime * speed);
        }
    }

    // Odwrócenie sprite'a NPCta wzdłuż osi X na podstawie tego gdzie znajduje się cel do którego się udaje
    void SetFlipX(GameObject target)
    {
        if (target.transform.position.x > gameObject.transform.position.x) spriteRenderer.flipX = true;
        else spriteRenderer.flipX = false;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // Gdy gracz wejdzie w "strefę wykrycia" NPCta, ten wykryje go i ustawi detectPlayer na true a także odwróci sprite
        if (collision.gameObject.CompareTag("Player"))
        {
            enemy.detectPlayer = true;

            SetFlipX(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("EnemyWaypoint"))
        {
            collisionWaypoint = collision.gameObject;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        // Gdy gracz ucieknie od NPCta, ten wraca do najbliższego waypointa a potem dalej do patrolowania
        if (collision.gameObject.CompareTag("Player"))
        {
            enemy.detectPlayer = false;
            waypointFinded = false;
            goToNearWaypoint = true;

            waypointTarget = enemy.FindNearWaypoint();
        }
    }

}
