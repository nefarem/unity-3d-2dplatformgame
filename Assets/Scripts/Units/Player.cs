﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Unit
{
    // Sloty w EQ
    public EquimentSlot[] eqSlots;
 
    private void OnEnable()
    {
        foreach (EquimentSlot item in eqSlots) item.keyCounter = 0;
    }

}
