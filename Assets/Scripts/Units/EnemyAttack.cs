﻿using System;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    [SerializeField] Transform attackPoint;

    float attackRange = 0.75f; // Zasięg ataku
    float cooldownAttack = 0.9f; // Długość przerwy między dwoma atakami

    [SerializeField] LayerMask playerLayer; // Maska

    float nextAttackTimer;      // Odliczanie do następnego ataku
    bool isAttackCoooldown = false;  // Czy trwa aktualnie przerwa między atakami

    Enemy enemy;
    Animator animator;
    Wizard wizard;

    void Start()
    {
        enemy = GetComponent<Enemy>();
        animator = GetComponent<Animator>();

        if (enemy.NPCClassEnumType == Enemy.NPCClass.mage) wizard = GetComponent<Wizard>();
    }

    void Update()
    {
        // Gdy gracz jest w zasięgu ataku NPC wywołana zostanie funkcja "Attack" odpowiedzialna za atak NPCta
        if (Vector2.Distance(transform.position, enemy.target.transform.position) <= attackRange && isAttackCoooldown == false && enemy.isDead == false)
            Attack();  // W przeciwnym razie isAttacking jest ustawione na false, czy NPC nie atakuje
        else if (Vector2.Distance(transform.position, enemy.target.transform.position) > attackRange && isAttackCoooldown == false && enemy.isDead == false)
            enemy.isAttacking = false;

        // Jeżeli NPC jest magiem i nie patroluje terenu to wykonuje ataki magiczne w postaci wystrzeliwywania kul ognia
        if (enemy.detectPlayer && enemy.isDead == false && isAttackCoooldown == false && enemy.NPCActionEnumType == Enemy.NPCAction.stay && enemy.NPCClassEnumType == Enemy.NPCClass.mage)
        {
            MagicAttack();
        }

        // Odliczanie przerwy między atakami
        if (isAttackCoooldown)
        {
            nextAttackTimer += Time.deltaTime;

            if (nextAttackTimer >= cooldownAttack)
            {
                isAttackCoooldown = false;
                nextAttackTimer = 0f;
            }
        }
    }

    // Atak wykonywany przez NPC
    void Attack()
    {
        enemy.isAttacking = true;
        isAttackCoooldown = true;

        animator.SetTrigger("Attack");

        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, playerLayer);

        foreach (var item in enemiesToDamage)
        {
            if (item.isTrigger == false)
            {
                item.GetComponent<Player>().TakeDamage(enemy.attackDamage);
            }

        }
    }

    // Atak magiczny który respi kulę ognia
    void MagicAttack()
    {
        enemy.isAttacking = true;
        isAttackCoooldown = true;

        animator.SetTrigger("Attack");

        Instantiate(wizard.fireballPrefab, wizard.fireballSpawn.position, Quaternion.identity);
    }

}
