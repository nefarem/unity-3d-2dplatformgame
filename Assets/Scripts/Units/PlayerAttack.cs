﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{

    public Transform attackPoint;

    public int attackDamage = 20; // Obrażenia

    public float attackRange = 0.27f; // Zasięg ataku

    public LayerMask enemyLayers; // Maska

    float nextAttackTimer; // Odliczanie do następnego ataku
    bool isAttackCoooldown; // Czy trwa aktualnie przerwa między atakami

    Player player;
    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        player = GetComponent<Player>();
    }

    void Update()
    {
        // Gdy gracz naciśnei lewy przycisk myszy i nie trwa przerwa między atakami to wykona atak
        if (Input.GetMouseButtonDown(0) && isAttackCoooldown == false)
        {
            Attack();
        }

        // Odliczanie przerwy między atakami
        if (isAttackCoooldown)
        {
            nextAttackTimer += Time.deltaTime;

            if (nextAttackTimer >= 0.9f)
            {
                player.isAttacking = false;
                isAttackCoooldown = false;

                nextAttackTimer = 0f;
            }
        }
    }

    // Atak wykonywany przez gracza
    void Attack()
    {
        player.isAttacking = true;
        isAttackCoooldown = true;

        animator.SetTrigger("Attack");

        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);

        foreach (var item in enemiesToDamage)
        {
            if (item.isTrigger == false)
            {
                item.GetComponent<Unit>().TakeDamage(attackDamage);
            }

        }
    }

}
