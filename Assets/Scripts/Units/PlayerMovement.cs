﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    Animator animator;
    Rigidbody2D rigidbody2D;
    Player player;

    public int jumpCounter = 0;

    public bool isNearWall = false;

    float timer = 2.0f;
    bool isOnGround = false;
    bool starTimer;
    bool leftWall;
    bool rightWall;

    void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();
        player = GetComponent<Player>();
    }

    void Update()
    {
        Move();
        Jump();
    }


    void Move()
    {
        if (player.isAttacking == false && player.isDead == false)
        {
            float inputX = Input.GetAxis("Horizontal");

            float inputHorizontal = Input.GetAxisRaw("Horizontal");

            // Poruszanie lewo/prawo
            rigidbody2D.velocity = new Vector2(inputHorizontal * 4f, rigidbody2D.velocity.y);
            
            // Przełączanie animacji między biegiem a staniem w miejscu
            if (Mathf.Abs(inputX) > Mathf.Epsilon) animator.SetInteger("AnimState", 2);
            else animator.SetInteger("AnimState", 0);

            // Odwracanie sprite'a gracza wzdłuż osi X w zależności od kierunku poruszania się
            if (inputX > 0) GetComponent<SpriteRenderer>().flipX = true;
            else if (inputX < 0) GetComponent<SpriteRenderer>().flipX = false;
        }

        // Przypisanie AirSpeed wartości velocity.y
        animator.SetFloat("AirSpeed", rigidbody2D.velocity.y);

        // WallJump i odliczanie trwania przesuniecia gracza który się odbija od ścian w zależności od której ściany odbija się gracz (lewa albo prawa ściana)
        if (starTimer && timer > 0f)
        {    
            if (leftWall) transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x + 4.0f, transform.position.y + 4.0f), Time.deltaTime * 2.0f);
            if (rightWall) transform.position = Vector2.MoveTowards(transform.position, new Vector2(transform.position.x - 4.0f, transform.position.y + 4.0f), Time.deltaTime * 2.0f);

            timer -= Time.deltaTime;
        }
        else
        {
            starTimer = false;
            timer = 2.0f;
        }

        animator.SetBool("Grounded", isOnGround);

        // Gdy gracz jest w powietrzu
        if (animator.GetFloat("AirSpeed") != 0f) isOnGround = false;
        else
        {
            isOnGround = true;
            jumpCounter = 0;
        }
    }

    void Jump()
    {
        //Jump pod spacją
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Uruchomienie timera odpowiedzialnego za przemieszczanie gracza który odbija się od ściany
            if (isNearWall && isOnGround == false)
            {
                starTimer = true;    
            }
            
            // Uniemożliwienie wykonywania więcej niz dwóch skoków
            if (jumpCounter != 2)
            {
                jumpCounter++;

                animator.SetTrigger("Jump");
                animator.SetBool("Grounded", isOnGround);

                isOnGround = false;

                rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 2.0f);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        // Gdy gracz dotknie ziemi 
        if (collision.gameObject.CompareTag("Ground"))
        {
            isOnGround = true;

            timer = 2.0f;
            starTimer = false;
        }

        // Gdy gracz dotknie ściany sprawdzana jest czy lewa albo prawa od gracza
        if (collision.gameObject.CompareTag("Wall"))
        {
            jumpCounter = 0;
            timer = 2.0f;

            isNearWall = true;

            if (collision.gameObject.transform.position.x > gameObject.transform.position.x)
            {
                rightWall = true;
                leftWall = false;
            }

            if (collision.gameObject.transform.position.x < gameObject.transform.position.x)
            {
                leftWall = true;
                rightWall = false;
            }
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        // Gdy odejdzie od ściany
        if (collision.gameObject.CompareTag("Wall")) isNearWall = false;
    }
}
