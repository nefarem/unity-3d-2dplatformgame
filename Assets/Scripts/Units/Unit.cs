﻿using UnityEngine;

public class Unit : MonoBehaviour
{
    public int hp; // HP
    public int attackDamage; // Obrażenia

    [HideInInspector]
    public bool isAttacking;

    [HideInInspector]
    public bool isDead;

    protected Rigidbody2D rigidbody_2D;
    protected Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody_2D = GetComponent<Rigidbody2D>();

        isAttacking = false;
    }

    // Zadanie obrażeń
    public void TakeDamage(int damage)
    {
        animator.SetTrigger("Hurt");
        hp -= damage;

        if (hp <= 0)
        {
            if (gameObject.CompareTag("Enemy")) gameObject.GetComponent<Enemy>().Die();
            else Die();
        }
       
    }
    
    // Ustawienie wartości HP
    public void SetHP(int number)
    {
        hp = number;
    }


    // Śmierć gracza 
    void Die()
    {
        isDead = true;

        SetHP(0);

        animator.SetTrigger("Death");

        rigidbody_2D.simulated = false;

        foreach (var item in GetComponents<Collider2D>()) Destroy(item);

    }
}
