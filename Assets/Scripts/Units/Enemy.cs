﻿using UnityEngine;

public class Enemy : Unit
{
    public enum NPCAction { stay, patrol} // Określa jaką akcje ma wykonywać NPC
    public NPCAction NPCActionEnumType;

    public enum NPCClass { warrior, mage } // Określa klasę NPCta
    public NPCClass NPCClassEnumType;

    [SerializeField] GameObject dropPrefab;

    [HideInInspector]
    public GameObject target;

    [HideInInspector]
    public bool detectPlayer;

    void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody_2D = GetComponent<Rigidbody2D>();

        target = GameObject.FindGameObjectsWithTag("Player")[0];
    }

    // Wyszukiwanie najbliższego waypointa do którego ma udać się NPC
    public GameObject FindNearWaypoint()
    {
        GameObject[] waypoints = GameObject.FindGameObjectsWithTag("EnemyWaypoint");

        GameObject nearWaypoint = waypoints[0];

        float minDistance = Vector2.Distance(gameObject.transform.position, waypoints[0].transform.position);

        for (int i = 1; i < waypoints.Length; i++)
        {
            if (Vector2.Distance(gameObject.transform.position, waypoints[i].transform.position) < minDistance)
            {
                nearWaypoint = waypoints[i];
                minDistance = Vector2.Distance(gameObject.transform.position, waypoints[i].transform.position);
            }
        }

        return nearWaypoint;
    }

    // Funkcja wywoływana po śmierci NPCta
    public void Die()
    {
        isDead = true;

        SetHP(0);

        animator.SetTrigger("Death");

        rigidbody_2D.simulated = false;

        foreach (var item in GetComponents<Collider2D>()) Destroy(item);

        Instantiate(dropPrefab, new Vector2(transform.position.x, transform.position.y + 0.164978f), Quaternion.identity);

        Destroy(gameObject, 5f);
    }
}
