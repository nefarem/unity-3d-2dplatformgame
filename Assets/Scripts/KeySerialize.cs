using UnityEngine;

[System.Serializable]
public class KeySerialize
{
    Vector2Serialize position;
    string keyName;

    public KeySerialize(Vector2Serialize _position, string _keyName)
    {
        position = _position;
        keyName = _keyName;
    }

    public string GetKeyName()
    {
        return keyName;
    }

    public Vector2 GetPosition()
    {
        return position.GetVector2();
    }
}
