﻿using UnityEngine;

[CreateAssetMenu(fileName = "New EQ slot", menuName = "EQ Slot")]
public class EquimentSlot : ScriptableObject
{
    public Sprite keyImage; // Obraz klucza

    public string keyName;  // Nazwa klucza (KeyGold, KeyBlack)

    public int keyCounter; // Liczba posiadanych kluczy danego typu
}
