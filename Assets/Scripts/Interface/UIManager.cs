﻿using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject informationPanel;
    public GameObject losePanel;

    public Text hpUIText;
    public Text moneyUIText;
    public Text informationUIText;

    int informationCounter = 0;

    string[] informations = {
        "Klawisze A/D lub strzałki Lewo/Prawo odpowiadają za poruszanie się w lewo i prawo.",
        "Klawisz Spacja odpowiada za skok, możliwe jest wykonanie podwójnego skoku.",
        "Aby wykonać walljump należy stanąć blisko ściany nacisnąć spację a następnie będąc w powietrzu przy ściane ponownie nacisnąć spację w efekcie czego postać odbije się od ściany.",
        "W grze można spotkać 2 przeciwników, wojownika oraz maga, aby ich zaatakować należy kliknąć lewy przycisk myszy.",
        "Każdy z przeciwników po swojej śmierci upuszcza złoto a liczbę zdobytego złota można zobaczyć na górze ekranu obok liczby życia.",
        "W grze można również podnieść potki na regenerację życia.",
        "Występują dwa typy przeszkód które można usunąć za pomocą klucza, klucz należy przeciągnąć z ekranu gry do odpowiedniego slotu w ekwipunku na dole. Aby wyjąć klucz z ekwipunku należy kliknąć na nim lewy przycisk myszy a następnie skierować kursor na przeszkodę.",
        "Życzę miłej gry :)"
    };

    Player player; 
    GameManager gameManager;

    int currentHP;
    int currentMoney;

    void Start()
    {
        gameManager = GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();

        informationUIText.text = informations[0];
    }

    // Aktualizowanie informacji na interfejsie (HP, monety) a także sprawdza czy gracz odczytał informacje początkową
    void Update()
    {
        if (player != null && (currentHP < player.hp || currentMoney < gameManager.money))
        {
            currentHP = player.hp;
            currentMoney = gameManager.money;

            hpUIText.text = player.hp.ToString();
            moneyUIText.text = gameManager.money.ToString();

            if (player.hp <= 0) losePanel.SetActive(true);
        }

        

        if (gameManager.showedInformation)
        {
            Time.timeScale = 0f;
            informationPanel.SetActive(true);
        }
        else
        {
            Time.timeScale = 1f;
            informationPanel.SetActive(false);
        }
    }

    // Zmienianie informacji na początku gry informujące o najważniejszych rzeczach
    public void NextInformation()
    {
        informationCounter++;

        if (informationCounter == 8)
        {
            gameManager.showedInformation = false;
            return;
        }

        informationUIText.text = informations[informationCounter];

    }

    // Resetowanie poziomu razem z skasowaniem zapisów
    public void RestartLevel()
    {
        if (File.Exists(Application.persistentDataPath + "/gamesave.save")) File.Delete(Application.persistentDataPath + "/gamesave.save");

        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    // Wyjście z gry wraz z zapisame postępu gracza
    public void ExitGame()
    {
        GetComponent<SaveGame>().SaveGameFunc();

        Application.Quit();
    }
}
