﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EQSlotDisplay : MonoBehaviour
{
    public EquimentSlot equimentSlot; //Odwołanie do scriptable object

    public GameObject keyPrefab;

    public Image keyImageUI;
    public Text keyCounterTextUI;

    GameManager gameManager;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        keyImageUI.sprite = equimentSlot.keyImage;
    }

    void Update()
    {
        // Aktualizowanie liczby kluczy w ekwipunku
        keyCounterTextUI.text = equimentSlot.keyCounter.ToString();
    }

    public void PointerEnter()
    {
        // Gdy zostanie przeciągnięty klucz z ekranu gry do ekwipunku, zostanie on tam dodany 
        if (gameManager.movingKey && gameManager.movingKeyObject.GetComponent<Key>().keyName == equimentSlot.keyName && gameManager.movingKeyObject.GetComponent<Key>().fromInventory == false)
        {
            equimentSlot.keyCounter++;

            Destroy(gameManager.movingKeyObject);

            gameManager.movingKeyObject = null;
        }
    }

    public void PointerClick()
    {
        // Gdy gracz posiada 1 lub więcej kluczy w ekwipunku zostanie on wyciągnięty z ekwipunktu w celu usunięcia przeszkody
        if (equimentSlot.keyCounter > 0)
        {
            gameManager.clickEQSlot = gameObject;
            gameManager.movingKey = true;

            gameManager.movingKeyObject = Instantiate(keyPrefab, gameManager.PlayerObject.transform.position, Quaternion.identity);

            gameManager.movingKeyObject.GetComponent<Key>().fromInventory = true;
        }
        
    }
}
