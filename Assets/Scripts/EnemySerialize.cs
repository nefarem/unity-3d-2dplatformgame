using System;
using UnityEngine;

[Serializable]
public class EnemySerialize
{
    int hp;

    Enemy.NPCAction nPCAction;
    Enemy.NPCClass nPCClass;
    
    public EnemySerialize(int _hp, Enemy.NPCAction _nPCAction, Enemy.NPCClass _nPCClass)
    {
        hp = _hp;
        nPCAction = _nPCAction;
        nPCClass = _nPCClass;
    }

    public int GetHP()
    {
        return hp;
    }

    public Enemy.NPCAction GetAction()
    {
        return nPCAction;
    }

    public Enemy.NPCClass GetEnemyClass()
    {
        return nPCClass;
    }

}
