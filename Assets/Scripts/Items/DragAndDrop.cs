﻿using System;
using UnityEngine;

public class DragAndDrop : MonoBehaviour
{
    Vector2 startPosition; // Początkowa pozycja klucza
    Vector3 mousePos;  // Pozycja myszki

    GameManager gameManager;

    void Start()
    {
        startPosition = transform.position;

        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
        mousePos.z = 0;

        RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero, Mathf.Infinity);

        // Gdy gracz najedzie i kliknie klucz zostanie on oznaczony jako ten który ma być przesuwany po ekranie gry
        if (hit.collider != null && Input.GetMouseButtonDown(0) && gameManager.movingKey == false)
        {
            if (hit.collider.gameObject.CompareTag("Key"))
            {
                gameManager.movingKey = true;
                gameManager.movingKeyObject = hit.collider.gameObject;
            }
        }

        // Przesuwanie klucza po ekraniu gry
        if (gameManager.movingKey && gameManager.movingKeyObject != null)
        {
            gameManager.movingKeyObject.transform.position = mousePos;
        }
    }

    void OnMouseUp()
    {
        // Gdy gracz puści lewy przycisk myszy:
        // Jeżeli klucz nie zostanie przesunięty do ekwipunku, wróci na swoje początkowe miejsce w grze
        // Jeżeli jest to klucz z ekwipunku, i nie zostanie przesunięty na przeszkodę to zostanie on skasowany (ale w ekwipunku nadal pozostanie)

        gameManager.movingKey = false;
        gameManager.movingKeyObject = null;

        if (GetComponent<Key>().fromInventory == false) transform.position = startPosition;
        else Destroy(gameObject);
        
    }
}
