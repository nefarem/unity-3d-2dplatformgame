﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    // Kula ognia która jest wystrzeliwywana przez maga

    Vector2 targetLastPos;

    float speed = 0.9f;

    void Start()
    {
        targetLastPos = GameObject.Find("Player").transform.position;
    }

    void Update()
    {
        // Przesuwanie kuli w stronę ostatniej pozycji gracza (nie podąża za nim)

        transform.position = Vector2.MoveTowards(gameObject.transform.position, new Vector2(targetLastPos.x, gameObject.transform.position.y), Time.deltaTime * speed);

        // Gdy kula dotrze na ostatnią znaną pozycję gracza to zostanie usunięta
        if (Vector2.Distance(gameObject.transform.position, new Vector2(targetLastPos.x, gameObject.transform.position.y)) < 0.01f)
            Destroy(gameObject);
    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        // Jeżeli kula dotknie gracza odejmie mu 50 hp i zostanie usunięta
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Player>().TakeDamage(50);
            Destroy(gameObject);
        }
    }
}
