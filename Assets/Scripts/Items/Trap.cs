﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    // Pułapka
    public string needKey; // Odpowiedni klucz który wymagany jest usunięcia przeszkody

    GameManager gameManager;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // Gdy gracz dotknie pułapki to od razu zginie
        if (collision.gameObject.CompareTag("Player")) collision.gameObject.GetComponent<Player>().TakeDamage(100);

        // Gdy klucz dotknie pułapki i jest to odpowiedni klucz, to przeszkoda zostanie usunięta
        if (collision.gameObject.CompareTag("Key"))
        {
            if (collision.gameObject.GetComponent<Key>().keyName == needKey && collision.gameObject.GetComponent<Key>().fromInventory)
            {
                gameManager.clickEQSlot.GetComponent<EQSlotDisplay>().equimentSlot.keyCounter--;

                Destroy(collision.gameObject);
                Destroy(gameObject);

                gameManager.movingKeyObject = null;
                gameManager.movingKey = false;
            }
        }
    }

}
