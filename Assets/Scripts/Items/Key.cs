﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public string keyName;  // Nazwa klucza (KeyGold, KeyBlack)
    public bool fromInventory; // Określa czy klucz był zabierany z ekwipunku czy nie
}
