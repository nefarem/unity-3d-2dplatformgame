﻿using UnityEngine;

public class PickUpItem : MonoBehaviour
{
    // Odpowiada za podniesienie przedmiotów takich jak monety i potki na życie

    public enum ItemEnum { money, hpPotion}
    public ItemEnum itemEnumType;

    [SerializeField]  AudioClip pickUpSong; // Dźwięk podniesienia przedmiotu

    [SerializeField] GameObject pickUpItemParticle; // Efekt cząsteczkowy

    GameManager gameManager;
    AudioSource audioSource;

    void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        audioSource = GetComponent<AudioSource>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // Podniesienie
        if (collision.gameObject.CompareTag("Player"))
        {
            switch (itemEnumType)
            {
                case ItemEnum.money:
                    gameManager.SetMoney(gameManager.money += 1); // Zwiększenie monet o 1
                    break;

                case ItemEnum.hpPotion:
                    collision.gameObject.GetComponent<Player>().SetHP(100); // Zregenerowania liczby życia gracza do pełna (100)
                    break;
            }
            
            audioSource.clip = pickUpSong;
            audioSource.Play();

            Instantiate(pickUpItemParticle, transform.position, Quaternion.identity);

            Destroy(gameObject, 1f);
        }
    }
}
