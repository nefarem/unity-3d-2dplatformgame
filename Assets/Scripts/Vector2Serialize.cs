using System;
using UnityEngine;

[Serializable]
public class Vector2Serialize
{
    float x;
    float y;

    public Vector2Serialize(float _x, float _y)
    {
        x = _x;
        y = _y;
    }

    public Vector2 GetVector2()
    {
        return new Vector2(x, y);
    }
}
