using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TrapSerialize
{
    Vector2Serialize position;
    string needKey;

    public TrapSerialize(Vector2Serialize _position, string _needKey)
    {
        position = _position;
        needKey = _needKey;
    }

    public string GetNeedKey()
    {
        return needKey;
    }

    public Vector2 GetPosition()
    {
        return position.GetVector2();
    }
}
