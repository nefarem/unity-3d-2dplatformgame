using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class LoadGame : MonoBehaviour
{
    // Wczytywanie gry
    // Ponieważ nie wiedziałem jak zrobić by usuwać z gry obiekty które zostały podniesione przez gracza (lub zabite jeśli to bandyci, magowie)
    // to zrobiłem tak że usuwane są wszystkie obiekty za pomocą funkcji 'DeleteAllObjects();', a potem zapisane obiekty są umieszczane na planszy

    public GameObject warriorEnemyPrefab;
    public GameObject mageEnemyPrefab;
    public GameObject potionPrefab;
    public GameObject trapPrefab;
    public GameObject keyGoldPrefab;
    public GameObject keyBlackPrefab;

    public void LoadGameFunc()
    {
        // Wczytywanie pliku i tworzenie obiektu klasy SaveData
        if (File.Exists(Application.persistentDataPath + "/gamesave.save"))
        {   
            DeleteAllObjects();

            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
            Save saveData = (Save)bf.Deserialize(file);
            file.Close();

            GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
            playerObject.transform.position = saveData.playerPosition.GetVector2();

            Player player = playerObject.GetComponent<Player>();
            
            player.SetHP(saveData.playerHp);
            player.eqSlots[0].keyCounter = saveData.obtainedGoldKeys;
            player.eqSlots[1].keyCounter = saveData.obtainedBlackKeys;

            int i = 0;

            foreach (var item in saveData.enemyList)
            {
                switch (item.GetEnemyClass())
                {
                    case Enemy.NPCClass.warrior:
                        GameObject enemyObject = Instantiate(warriorEnemyPrefab, saveData.enemyPositions[i].GetVector2(), Quaternion.identity);
                        enemyObject.GetComponent<Enemy>().NPCActionEnumType = item.GetAction();
                    break;

                    case Enemy.NPCClass.mage:
                        GameObject mageEnemyObject = Instantiate(mageEnemyPrefab, saveData.enemyPositions[i].GetVector2(), Quaternion.identity);
                        mageEnemyObject.GetComponent<Enemy>().NPCActionEnumType = item.GetAction();
                    break;
                }

                i++;
            }

            i = 0;

            foreach (var item in saveData.itemPositions) Instantiate(potionPrefab, item.GetVector2(), Quaternion.identity);

            foreach (var item in saveData.keys)
            {
                switch (item.GetKeyName())
                {
                    case "KeyGold":
                        Instantiate(keyGoldPrefab, item.GetPosition(), Quaternion.identity);
                    break;

                    case "KeyBlack":
                        Instantiate(keyBlackPrefab, item.GetPosition(), Quaternion.identity);
                    break;
                }
            }

            foreach (var item in saveData.traps)
            {
                GameObject trapObject = Instantiate(trapPrefab, item.GetPosition(), Quaternion.identity);
                trapObject.GetComponent<Trap>().needKey = item.GetNeedKey();
            }
            GetComponent<GameManager>().money = saveData.money;

            
            // Debug.Log("Loaded!");
        }

    }

    void DeleteAllObjects()
    {
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Enemy")) Destroy(item);
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Potion")) Destroy(item);
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Key")) Destroy(item);
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Trap")) Destroy(item);
    }
}
