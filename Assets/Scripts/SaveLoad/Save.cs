﻿using System.Collections.Generic;
using UnityEngine;

// Klasa przechowująca informacje do zapisu stanu gry
[System.Serializable]
public class Save
{
    public Vector2Serialize playerPosition;
    public int playerHp;

    public List<EnemySerialize> enemyList = new List<EnemySerialize>();
    public List<Vector2Serialize> enemyPositions = new List<Vector2Serialize>();

    public List<Vector2Serialize> itemPositions = new List<Vector2Serialize>(); // Potki
    public List<KeySerialize> keys = new List<KeySerialize>(); // Klucze
    public List<TrapSerialize> traps = new List<TrapSerialize>(); // Pułapki

    public List<string> itemName = new List<string>();
    public List<int> itemCounter = new List<int>();


    public int money;
    public int obtainedGoldKeys;
    public int obtainedBlackKeys;
}