﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class SaveGame : MonoBehaviour
{

    // Zapis gry
    public void SaveGameFunc()
    {
        Save saveData = Save();

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save");
        bf.Serialize(file, saveData);
        file.Close();

       // Debug.Log("Saved!");
    }

    Save Save()
    {
        Save saveData = new Save();

        GameObject playerObject = GameObject.FindGameObjectWithTag("Player");

        // Zapisywanie danych o obiektach
        saveData.playerPosition = new Vector2Serialize(playerObject.transform.position.x, playerObject.transform.position.y);
        Player player = playerObject.GetComponent<Player>();
        
        saveData.playerHp = player.hp;
        saveData.obtainedGoldKeys = player.eqSlots[0].keyCounter;
        saveData.obtainedBlackKeys = player.eqSlots[1].keyCounter;

        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Enemy enemy = item.GetComponent<Enemy>();

            saveData.enemyList.Add(new EnemySerialize(enemy.hp, enemy.NPCActionEnumType, enemy.NPCClassEnumType));
            saveData.enemyPositions.Add(new Vector2Serialize(item.transform.position.x, item.transform.position.y));
        }

        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Potion"))
        {
            saveData.itemPositions.Add(new Vector2Serialize(item.transform.position.x, item.transform.position.y));
        }

        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Key"))
        {
            saveData.keys.Add(new KeySerialize((new Vector2Serialize(item.transform.position.x, item.transform.position.y)), item.GetComponent<Key>().keyName));
        }

        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Trap"))
        {
            saveData.traps.Add(new TrapSerialize(new Vector2Serialize(item.transform.position.x, item.transform.position.y), item.GetComponent<Trap>().needKey));
        }

        saveData.money = GetComponent<GameManager>().money;

        return saveData;
    }

}
