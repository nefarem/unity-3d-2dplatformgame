﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int money; // Monety zebrane przez graccza

    public GameObject PlayerObject; // Referencja do gracza
    public GameObject movingKeyObject; // Referencja do przesuwanego klucza
    public GameObject clickEQSlot; // Kliknięty slot w EQ

    public bool movingKey; // Czy klucz jest przesuwany
    public bool showedInformation = true; // Czy informacja początkowa została wyświetlona

    void Start()
    {
        PlayerObject = GameObject.FindGameObjectsWithTag("Player")[0];

        // Wczytywanie gry
        GetComponent<LoadGame>().LoadGameFunc();
    }

    // Ustalenie liczby monet
    public void SetMoney(int x)
    {
        money = x;
    }


}
